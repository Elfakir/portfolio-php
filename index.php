<?php include("abonner.php");?>

<!DOCTYPE HTML>
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Template</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="" />

  <!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="favicon.ico">

	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800" rel="stylesheet">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<!-- Owl Carousel -->
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">
	<!-- Magnific Popup -->
	<link rel="stylesheet" href="css/magnific-popup.css">

	<link rel="stylesheet" href="css/style.css">


	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>

	<nav id="colorlib-main-nav" role="navigation">
		<a href="#" class="js-colorlib-nav-toggle colorlib-nav-toggle active"><i></i></a>
		<div class="js-fullheight colorlib-table">
			<div class="colorlib-table-cell js-fullheight">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<ul>
								<li class="active"><a href="index.php"><span>Home</span></a></li>
								<li><a href="about.html"><span>A prepos</span></a></li>
								<li><a href="services.php"><span>Services</span></a></li>
								<li><a href="work.php"><span>Travail</span></a></li>
								<!--<li><a href="blog.html"><span>Blog</span></a></li>-->
								<li><a href="contact.php"><span>Contact</span></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</nav>
	
	<div id="colorlib-page">
		<header>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="colorlib-navbar-brand">
							<a class="colorlib-logo" href="index.php">M.</a>
						</div>
						<a href="#" class="js-colorlib-nav-toggle colorlib-nav-toggle"><i></i></a>
					</div>
				</div>
			</div>
		</header>
		<div id="colorlib-hero" class="js-fullheight">
			<p class="social-media">
				
				<a href="#">Facebook</a>
				<a href="#">Instagram</a>
				
			</p>
			<div class="owl-carousel">
				<div class="item">
					<div class="hero-flex js-fullheight">
						<div class="col-three-forth">
							<div class="hero-img js-fullheight" style="background-image: url(images/img_bg_1.jpeg);"></div>
						</div>
						<div class="col-one-forth js-fullheight">
							<div class="display-t js-fullheight">
								<div class="display-tc js-fullheight">
									<div class="text-inner">
										<div class="desc">
											<h2>Je suis Elfakir,developeur de maroc.</h2>
											<p><a href="work.html" class="btn-view">je suis la <i class="icon-arrow-right3"></i></a></p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="hero-flex js-fullheight">
						<div class="col-three-forth">
							<div class="hero-img js-fullheight" style="background-image: url(images/img_bg_2.jpeg);"></div>
						</div>
						<div class="col-one-forth js-fullheight">
							<div class="display-t js-fullheight">
								<div class="display-tc js-fullheight">
									<div class="text-inner">
										<div class="desc">
											<h2>Je suis un programmeur de produits numériques basé à YOUSSOUFIA..</h2>
											<p><a href="work.php" class="btn-view">Je suis la <i class="icon-arrow-right3"></i></a></p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="colorlib-services">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-md-offset-0 text-center animate-box intro-heading">
						<span>Ce que je fais</span>
						<h2>Stratégie de programmeur, et un peu de magie</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4 text-center animate-box">
						<div class="services">
							<span class="icon">
								<i class="icon-magnifying-glass"></i>
							</span>
							<div class="desc">
								<h3><a href="#">HTML</a></h3>
								<h4>Create CV</h4>
								<h4>Page web</h4>
								<h4>.....</h4>
							</div>
						</div>
					</div>
					<div class="col-md-4 text-center animate-box">
						<div class="services">
							<span class="icon">
								<i class="icon-layers"></i>
							</span>
							<div class="desc">
								<h3><a href="#">CSS</a></h3>
								<h4>Flexbox</h4>
								<h4>Grid system</h4>
								<h4>Key Frame</h4>
							</div>
						</div>
					</div>
					<div class="col-md-4 text-center animate-box">
						<div class="services">
							<span class="icon">
								<i class="icon-lightbulb"></i>
							</span>
							<div class="desc">
								<h3><a href="#">PHP</a></h3>
								<h4>CREATION A TABLE</h4>
								<h4>CONNECT TO THE DB</h4>
								<h4>SENDING FORMATION DB</h4>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="colorlib-case colorlib-bg-white">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-md-offset-0 text-center animate-box intro-heading">
						<span>Travail</span>
						<h2>Heureux de passer mon temps a ces projets</h2>
					</div>
				</div>
				<div class="work-wrap">
					<div class="row animate-box">
						<div class="owl-carousel1">
							<div class="item">
								<div class="col-md-6">
									<div class="case-img" style="background-image: url(images/work-1.jpg);"></div>
								</div>
								<div class="col-md-6">
									<div class="case-desc">
										<p class="tag"><span>UI/UX, Direction artistique</span></p>
										<h3><a href="#">Gestionnaire de CV site web playtime</a></h3>
										<p>Création d'un CV avec HTML et CSS</p>
										<p><a href="work.html" class="btn btn-primary">Voir détails</a></p>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="col-md-6">
									<div class="case-img" style="background-image: url(images/work-2.jpg);"></div>
								</div>
								<div class="col-md-6">
									<div class="case-desc">
										<p class="tag"><span>UI/UX, Direction artistique</span></p>
										<h3><a href="#">Gestionnaire de site web playtime</a></h3>
										<p>Création d'un site nomé challange pour pratiqué le HTML et CSS et s'avoir leur utulité</p>
										<p><a href="#" class="btn btn-primary">Voir détails</a></p>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="col-md-6">
									<div class="case-img" style="background-image: url(images/work-3.jpg);"></div>
								</div>
								<div class="col-md-6">
									<div class="case-desc">
										<p class="tag"><span>UI/UX, Direction artistique</span></p>
										<h3><a href="#">Application Mobile</a></h3>
										<p>Création d'une Application Mobile </p>
										<p><a href="#" class="btn btn-primary">Voir détails</a></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="colorlib-subscribe">
			<div class="overlay"></div>
			<div class="container">
				<div class="row row-padded-bottom">
					<div class="col-md-8 col-md-offset-2 text-center animate-box">
						<p>When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove, the headline of Alphabet Village and the subline of her own road, the Line Lane. Pityful a rethoric question ran over her cheek, then she continued her way.</p>
						<p>Even the all-powerful <strong>Pointing</strong> has no <strong>control about the blind texts it is an almost unorthographic</strong> life One day however a small line of blind text</p>
						<p><a href="#" class="resume"><i class="icon-documents"></i> Lisez mon resume ici</a></p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center colorlib-heading animate-box">
						<h2>S'abonner a la Newsletter</h2>
						<p>Abonnez-vous a notre newsletter et recevez les dérniers nouvelles</p>
					</div>
				</div>
				<div class="row animate-box">
					<div class="col-md-6 col-md-offset-3">
						<div class="row">
							<div class="col-md-12">
								<form action="#" method="POST" class="form-inline qbstp-header-subscribe">
									<div class="col-three-forth">
										<div class="form-group">
											<input type="text" name="mail" class="form-control" id="email" placeholder="Entrer votre email">
										</div>
									</div>
									<div class="col-one-third">
										<div class="form-group">
											<button type="submit" class="btn btn-primary">Abonnez-vous</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<footer class="colorlib-footer">
			<div class="container">
				<div class="row row-padded-bottom">
					<div class="col-md-6">
						<h3>Parlons</h3>
						<p>Être un programmeur,c'est génial, mais intégrer sa créativité, c'est quelque chose qui dépasse l'imagination</p>
						<p class="btn-footer"><a href="work.php">Parlez nous de votre projet</a></p>
					</div>
					<div class="col-md-6 info">
						<h3>Info</h3>
						<p>Email: <strong><a href="#">abdelhakf95@gmail.com</a></strong></p>
						<p>Phone: <strong><a href="#">(+212) 625874640</a></strong></p>
						<p>Address: <strong>province youssoufia, <br>  sidi ahmed</strong></p>
						<p class="colorlib-social-icons">
							<span><a href="#"><i class="icon-social-facebook"></i>Facebook</a></span>
							<span><a href="#"><i class="icon-social-twitter"></i>Twitter</a></span>
							<!--<span><a href="#"><i class="icon-social-dribbble"></i>Dribbble</a></span>-->
						</p>
					</div>
				</div>
			</div>
			<div class="container-fluid">
				<div class="row copyright">
					<div class="col-md-4 col-md-offset-4 text-center">
						<p>
							<small class="block">&copy; <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved <i class="icon-heart" aria-hidden="true"></i></small> 
							
						</p>
					</div>
				</div>
			</div>
		</footer>
	</div>

	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Owl Carousel -->
	<script src="js/owl.carousel.min.js"></script>
	<!-- Magnific Popup -->
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/magnific-popup-options.js"></script>

	<!-- Main JS (Do not remove) -->
	<script src="js/main.js"></script>

	

	</body>
</html>

